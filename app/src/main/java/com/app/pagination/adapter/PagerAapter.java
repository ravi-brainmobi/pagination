package com.app.pagination.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.app.pagination.fragments.MapFragment;
import com.app.pagination.fragments.MovieFragment;

/**
 * Created by Ravi on 02-01-2018.
 */

public class PagerAapter extends FragmentStatePagerAdapter {

    int numOfTabs;

    public PagerAapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0 :
                MovieFragment movieFragment = new MovieFragment();
                return movieFragment;
            case 1 :
                MapFragment mapFragment = new MapFragment();
                return mapFragment;
            default :
                return null;
        }
        }



    @Override
    public int getCount() {
        return numOfTabs;
    }
}
